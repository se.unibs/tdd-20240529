package it.unibs.ing.se.tdd.bg;

import java.time.LocalDate;

public class Employee {
    private final LocalDate birthDay;

    public Employee(String firstName, String lastName, LocalDate birthDay, String mail) {
        this.birthDay = birthDay;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }
}
