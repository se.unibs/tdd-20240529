package it.unibs.ing.se.tdd.bg;


import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;


public class Monolith {
    public static void main(String[] args) throws IOException, URISyntaxException {
        var rows = Files.readAllLines(Path.of(Monolith.class.getResource("/employees.txt").toURI()));
        for(var i = 0; i < rows.size(); i++) {
            if(i != 0) {
                var fields = rows.get(i).split(",");
                var dateAsText = fields[2];
                var birthday = LocalDate.parse(dateAsText.trim());
                var today = LocalDate.now();
                if(today.getMonth() == birthday.getMonth() && today.getDayOfMonth() == birthday.getDayOfMonth()) {
                    System.out.println(String.format("Happy birthday, %s (to be sent to %s)", fields[1], fields[3]));
                }
            }
        }
    }

}
