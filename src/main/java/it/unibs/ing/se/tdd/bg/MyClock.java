package it.unibs.ing.se.tdd.bg;

import java.time.LocalDate;

public interface MyClock {
    LocalDate today();
}
