package it.unibs.ing.se.tdd.bg;

import java.time.LocalDate;
import java.util.Collection;

public class BirthdayGreetingsService {
    private final MyClock clock;
    private final EmployeeProvider provider;
    private final GreetingsNotifier notifier;

    public BirthdayGreetingsService(MyClock clock, EmployeeProvider provider, GreetingsNotifier notifier) {

        this.clock = clock;
        this.provider = provider;
        this.notifier = notifier;
    }

    public void run() {
        LocalDate today = clock.today();
        Collection<Employee> employees = provider.getAllEmployees();
        for (var e : employees) {
            LocalDate birthDay = e.getBirthDay();
            if(birthDay.getDayOfMonth() == today.getDayOfMonth() && birthDay.getMonth() == today.getMonth()) {
                notifier.notifyBirthdayGreetings(e);
            }
        }
    }
}
