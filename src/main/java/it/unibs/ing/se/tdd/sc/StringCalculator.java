package it.unibs.ing.se.tdd.sc;

public class StringCalculator {
    private final String separator;

    public StringCalculator(String separator) {
        this.separator = separator;
    }

    public StringCalculator() {
        this(" ");
    }

    public int add(String input) {
        if (input == "") {
            return 0;
        }
        var fields = input.split(this.separator);
        var sum = 0;
        for (var f : fields) {
            try {
                sum += Integer.parseInt(f);
            } catch (Exception ex) {
                throw new InvalidInputException();
            }
        }
        return sum;
    }
}
