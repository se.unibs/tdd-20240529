package it.unibs.ing.se.tdd.bg;

import java.util.Collection;

public interface EmployeeProvider {
    Collection<Employee> getAllEmployees();
}
