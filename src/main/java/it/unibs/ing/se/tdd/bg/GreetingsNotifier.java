package it.unibs.ing.se.tdd.bg;

public interface GreetingsNotifier {
    void notifyBirthdayGreetings(Employee e);
}
