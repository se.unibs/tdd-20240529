package it.unibs.ing.se.tdd.bg;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class BirthdayGreetingsServiceTest {
    @Test
    public void foo() {
        MyClock clock = new FixedClock(LocalDate.of(2025, 3, 19));
        EmployeeProvider provider = new InMemoryProvider(List.of(
                new Employee("Pietro", "Martinelli", LocalDate.of(1978, 3, 19), "pietrom@example.com"),
                new Employee("Irene", "Martinelli", LocalDate.of(2011, 4, 19), "irene@example.com"),
                new Employee("Laura", "Martinelli", LocalDate.of(2013, 4, 19), "laura@example.com")
        ));
        GreetingsNotifier notifier = new SpyNotifier();
        var service = new BirthdayGreetingsService(clock, provider, notifier);

        service.run();

        assertThat(((SpyNotifier)notifier).getNotificationCount(), is(equalTo(1)));
    }
}

class SpyNotifier implements GreetingsNotifier {
    private int count;
    public int getNotificationCount() {
        return count;
    }

    @Override
    public void notifyBirthdayGreetings(Employee e) {
        count++;
    }
}

class FixedClock implements MyClock {
    private final LocalDate fixedDate;

    public FixedClock(LocalDate fixedDate) {
        this.fixedDate = fixedDate;
    }

    @Override
    public LocalDate today() {
        return fixedDate;
    }
}

class InMemoryProvider implements EmployeeProvider {
    private final List<Employee> employees;

    public <E> InMemoryProvider(List<Employee> employees) {
        this.employees = employees;
    }

    @Override
    public Collection<Employee> getAllEmployees() {
        return employees;
    }
}
