package it.unibs.ing.se.tdd;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class HelloTest {
    @Test
    public void shouldReturnHelloWorld() {
        assertThat(new Hello().sayHello(), is(equalTo("Hello, World!")));
    }
}
