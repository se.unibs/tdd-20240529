package it.unibs.ing.se.tdd.sc;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class StringCalculatorTest {
    @Test
    public void shouldReturnZeroWhenInputIsEmpty() {
        assertThat(new StringCalculator().add(""), is(equalTo(0)));
    }

    @Test
    public void shouldHandleSingleNumberProperly() {
        assertThat(new StringCalculator().add("19"), is(equalTo(19)));
    }

    @Test
    public void shouldHandleMoreThanOneNumberProperly() {
        assertThat(new StringCalculator().add("19 11 22 17"), is(equalTo(69)));
    }

    @Test
    public void canUseCustomSeparator() {
        assertThat(new StringCalculator(";").add("19;11;22;17"), is(equalTo(69)));
    }

    @Test
    public void shouldThrowInvalidInputExceptionWhenInputIsNotNumeric() {
        assertThrows(InvalidInputException.class, () -> new StringCalculator().add("19 11 pietrom 22"));
    }
}
